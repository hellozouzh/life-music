class UserInfoModel {
  int loginType;
  int code;
  Account account;
  String token;
  Profile profile;
  List<Bindings> bindings;
  String cookie;

  UserInfoModel(
      {this.loginType,
        this.code,
        this.account,
        this.token,
        this.profile,
        this.bindings,
        this.cookie});

  UserInfoModel.fromJson(Map<String, dynamic> json) {
    loginType = json['loginType'];
    code = json['code'];
    account =
    json['account'] != null ? new Account.fromJson(json['account']) : null;
    token = json['token'];
    profile =
    json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
    if (json['bindings'] != null) {
      bindings = new List<Bindings>();
      json['bindings'].forEach((v) {
        bindings.add(new Bindings.fromJson(v));
      });
    }
    cookie = json['cookie'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['loginType'] = this.loginType;
    data['code'] = this.code;
    if (this.account != null) {
      data['account'] = this.account.toJson();
    }
    data['token'] = this.token;
    if (this.profile != null) {
      data['profile'] = this.profile.toJson();
    }
    if (this.bindings != null) {
      data['bindings'] = this.bindings.map((v) => v.toJson()).toList();
    }
    data['cookie'] = this.cookie;
    return data;
  }
}

class Account {
  int id;
  String userName;
  int type;
  int status;
  int whitelistAuthority;
  int createTime;
  String salt;
  int tokenVersion;
  int ban;
  int baoyueVersion;
  int donateVersion;
  int vipType;
  int viptypeVersion;
  bool anonimousUser;

  Account(
      {this.id,
        this.userName,
        this.type,
        this.status,
        this.whitelistAuthority,
        this.createTime,
        this.salt,
        this.tokenVersion,
        this.ban,
        this.baoyueVersion,
        this.donateVersion,
        this.vipType,
        this.viptypeVersion,
        this.anonimousUser});

  Account.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userName = json['userName'];
    type = json['type'];
    status = json['status'];
    whitelistAuthority = json['whitelistAuthority'];
    createTime = json['createTime'];
    salt = json['salt'];
    tokenVersion = json['tokenVersion'];
    ban = json['ban'];
    baoyueVersion = json['baoyueVersion'];
    donateVersion = json['donateVersion'];
    vipType = json['vipType'];
    viptypeVersion = json['viptypeVersion'];
    anonimousUser = json['anonimousUser'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userName'] = this.userName;
    data['type'] = this.type;
    data['status'] = this.status;
    data['whitelistAuthority'] = this.whitelistAuthority;
    data['createTime'] = this.createTime;
    data['salt'] = this.salt;
    data['tokenVersion'] = this.tokenVersion;
    data['ban'] = this.ban;
    data['baoyueVersion'] = this.baoyueVersion;
    data['donateVersion'] = this.donateVersion;
    data['vipType'] = this.vipType;
    data['viptypeVersion'] = this.viptypeVersion;
    data['anonimousUser'] = this.anonimousUser;
    return data;
  }
}

class Profile {
  int birthday;
  int avatarImgId;
  String nickname;
  String description;
  int userId;
  int city;
  bool mutual;
  Null remarkName;
  Null expertTags;
  int authStatus;
  int userType;
  int backgroundImgId;
  String avatarUrl;
  bool defaultAvatar;
  int province;
  int djStatus;
  int vipType;
  int gender;
  int accountStatus;
  String avatarImgIdStr;
  String backgroundImgIdStr;
  bool followed;
  String backgroundUrl;
  String detailDescription;
  String signature;
  int authority;
  int followeds;
  int follows;
  int eventCount;
  int playlistCount;
  int playlistBeSubscribedCount;

  Profile(
      {this.birthday,
        this.avatarImgId,
        this.nickname,
        this.description,
        this.userId,
        this.city,
        this.mutual,
        this.remarkName,
        this.expertTags,
        this.authStatus,
        this.userType,
        this.backgroundImgId,
        this.avatarUrl,
        this.defaultAvatar,
        this.province,
        this.djStatus,
        this.vipType,
        this.gender,
        this.accountStatus,
        this.avatarImgIdStr,
        this.backgroundImgIdStr,
        this.followed,
        this.backgroundUrl,
        this.detailDescription,
        this.signature,
        this.authority,
        this.followeds,
        this.follows,
        this.eventCount,
        this.playlistCount,
        this.playlistBeSubscribedCount});

  Profile.fromJson(Map<String, dynamic> json) {
    birthday = json['birthday'];
    avatarImgId = json['avatarImgId'];
    nickname = json['nickname'];
    description = json['description'];
    userId = json['userId'];
    city = json['city'];
    mutual = json['mutual'];
    remarkName = json['remarkName'];
    expertTags = json['expertTags'];
    authStatus = json['authStatus'];
    userType = json['userType'];
    backgroundImgId = json['backgroundImgId'];
    avatarUrl = json['avatarUrl'];
    defaultAvatar = json['defaultAvatar'];
    province = json['province'];
    djStatus = json['djStatus'];
    vipType = json['vipType'];
    gender = json['gender'];
    accountStatus = json['accountStatus'];
    avatarImgIdStr = json['avatarImgIdStr'];
    backgroundImgIdStr = json['backgroundImgIdStr'];
    followed = json['followed'];
    backgroundUrl = json['backgroundUrl'];
    detailDescription = json['detailDescription'];
    signature = json['signature'];
    authority = json['authority'];
    followeds = json['followeds'];
    follows = json['follows'];
    eventCount = json['eventCount'];
    playlistCount = json['playlistCount'];
    playlistBeSubscribedCount = json['playlistBeSubscribedCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['birthday'] = this.birthday;
    data['avatarImgId'] = this.avatarImgId;
    data['nickname'] = this.nickname;
    data['description'] = this.description;
    data['userId'] = this.userId;
    data['city'] = this.city;
    data['mutual'] = this.mutual;
    data['remarkName'] = this.remarkName;
    data['expertTags'] = this.expertTags;
    data['authStatus'] = this.authStatus;
    data['userType'] = this.userType;
    data['backgroundImgId'] = this.backgroundImgId;
    data['avatarUrl'] = this.avatarUrl;
    data['defaultAvatar'] = this.defaultAvatar;
    data['province'] = this.province;
    data['djStatus'] = this.djStatus;
    data['vipType'] = this.vipType;
    data['gender'] = this.gender;
    data['accountStatus'] = this.accountStatus;
    data['avatarImgIdStr'] = this.avatarImgIdStr;
    data['backgroundImgIdStr'] = this.backgroundImgIdStr;
    data['followed'] = this.followed;
    data['backgroundUrl'] = this.backgroundUrl;
    data['detailDescription'] = this.detailDescription;
    data['signature'] = this.signature;
    data['authority'] = this.authority;
    data['followeds'] = this.followeds;
    data['follows'] = this.follows;
    data['eventCount'] = this.eventCount;
    data['playlistCount'] = this.playlistCount;
    data['playlistBeSubscribedCount'] = this.playlistBeSubscribedCount;
    return data;
  }
}

class Bindings {
  int bindingTime;
  bool expired;
  int userId;
  String tokenJsonStr;
  int expiresIn;
  int refreshTime;
  int id;
  int type;

  Bindings(
      {this.bindingTime,
        this.expired,
        this.userId,
        this.tokenJsonStr,
        this.expiresIn,
        this.refreshTime,
        this.id,
        this.type});

  Bindings.fromJson(Map<String, dynamic> json) {
    bindingTime = json['bindingTime'];
    expired = json['expired'];
    userId = json['userId'];
    tokenJsonStr = json['tokenJsonStr'];
    expiresIn = json['expiresIn'];
    refreshTime = json['refreshTime'];
    id = json['id'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bindingTime'] = this.bindingTime;
    data['expired'] = this.expired;
    data['userId'] = this.userId;
    data['tokenJsonStr'] = this.tokenJsonStr;
    data['expiresIn'] = this.expiresIn;
    data['refreshTime'] = this.refreshTime;
    data['id'] = this.id;
    data['type'] = this.type;
    return data;
  }
}