// To parse this JSON data, do
//
//     final songListModel = songListModelFromJson(jsonString);

import 'dart:convert';

SongListModel songListModelFromJson(String str) => SongListModel.fromJson(json.decode(str));

String songListModelToJson(SongListModel data) => json.encode(data.toJson());

class SongListModel {
  SongListModel({
    this.code,
    this.relatedVideos,
    this.playlist,
    this.urls,
    this.privileges,
  });

  int code;
  dynamic relatedVideos;
  Playlist playlist;
  dynamic urls;
  List<Privilege> privileges;

  factory SongListModel.fromJson(Map<String, dynamic> json) => SongListModel(
    code: json["code"],
    relatedVideos: json["relatedVideos"],
    playlist: Playlist.fromJson(json["playlist"]),
    urls: json["urls"],
    privileges: List<Privilege>.from(json["privileges"].map((x) => Privilege.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "relatedVideos": relatedVideos,
    "playlist": playlist.toJson(),
    "urls": urls,
    "privileges": List<dynamic>.from(privileges.map((x) => x.toJson())),
  };
}

class Playlist {
  Playlist({
    this.subscribers,
    this.subscribed,
    this.creator,
    this.tracks,
    this.videoIds,
    this.videos,
    this.trackIds,
    this.updateFrequency,
    this.backgroundCoverId,
    this.backgroundCoverUrl,
    this.titleImage,
    this.titleImageUrl,
    this.englishTitle,
    this.opRecommend,
    this.description,
    this.ordered,
    this.tags,
    this.coverImgId,
    this.coverImgUrl,
    this.newImported,
    this.updateTime,
    this.specialType,
    this.trackCount,
    this.commentThreadId,
    this.privacy,
    this.trackUpdateTime,
    this.trackNumberUpdateTime,
    this.playCount,
    this.subscribedCount,
    this.cloudTrackCount,
    this.status,
    this.userId,
    this.createTime,
    this.highQuality,
    this.adType,
    this.name,
    this.id,
    this.shareCount,
    this.coverImgIdStr,
    this.commentCount,
  });

  List<Creator> subscribers;
  bool subscribed;
  Creator creator;
  List<Track> tracks;
  dynamic videoIds;
  dynamic videos;
  List<TrackId> trackIds;
  dynamic updateFrequency;
  int backgroundCoverId;
  dynamic backgroundCoverUrl;
  int titleImage;
  dynamic titleImageUrl;
  dynamic englishTitle;
  bool opRecommend;
  String description;
  bool ordered;
  List<String> tags;
  double coverImgId;
  String coverImgUrl;
  bool newImported;
  int updateTime;
  int specialType;
  int trackCount;
  String commentThreadId;
  int privacy;
  int trackUpdateTime;
  int trackNumberUpdateTime;
  int playCount;
  int subscribedCount;
  int cloudTrackCount;
  int status;
  int userId;
  int createTime;
  bool highQuality;
  int adType;
  String name;
  int id;
  int shareCount;
  String coverImgIdStr;
  int commentCount;

  factory Playlist.fromJson(Map<String, dynamic> json) => Playlist(
    subscribers: List<Creator>.from(json["subscribers"].map((x) => Creator.fromJson(x))),
    subscribed: json["subscribed"],
    creator: Creator.fromJson(json["creator"]),
    tracks: List<Track>.from(json["tracks"].map((x) => Track.fromJson(x))),
    videoIds: json["videoIds"],
    videos: json["videos"],
    trackIds: List<TrackId>.from(json["trackIds"].map((x) => TrackId.fromJson(x))),
    updateFrequency: json["updateFrequency"],
    backgroundCoverId: json["backgroundCoverId"],
    backgroundCoverUrl: json["backgroundCoverUrl"],
    titleImage: json["titleImage"],
    titleImageUrl: json["titleImageUrl"],
    englishTitle: json["englishTitle"],
    opRecommend: json["opRecommend"],
    description: json["description"],
    ordered: json["ordered"],
    tags: List<String>.from(json["tags"].map((x) => x)),
    coverImgId: json["coverImgId"].toDouble(),
    coverImgUrl: json["coverImgUrl"],
    newImported: json["newImported"],
    updateTime: json["updateTime"],
    specialType: json["specialType"],
    trackCount: json["trackCount"],
    commentThreadId: json["commentThreadId"],
    privacy: json["privacy"],
    trackUpdateTime: json["trackUpdateTime"],
    trackNumberUpdateTime: json["trackNumberUpdateTime"],
    playCount: json["playCount"],
    subscribedCount: json["subscribedCount"],
    cloudTrackCount: json["cloudTrackCount"],
    status: json["status"],
    userId: json["userId"],
    createTime: json["createTime"],
    highQuality: json["highQuality"],
    adType: json["adType"],
    name: json["name"],
    id: json["id"],
    shareCount: json["shareCount"],
    coverImgIdStr: json["coverImgId_str"],
    commentCount: json["commentCount"],
  );

  Map<String, dynamic> toJson() => {
    "subscribers": List<dynamic>.from(subscribers.map((x) => x.toJson())),
    "subscribed": subscribed,
    "creator": creator.toJson(),
    "tracks": List<dynamic>.from(tracks.map((x) => x.toJson())),
    "videoIds": videoIds,
    "videos": videos,
    "trackIds": List<dynamic>.from(trackIds.map((x) => x.toJson())),
    "updateFrequency": updateFrequency,
    "backgroundCoverId": backgroundCoverId,
    "backgroundCoverUrl": backgroundCoverUrl,
    "titleImage": titleImage,
    "titleImageUrl": titleImageUrl,
    "englishTitle": englishTitle,
    "opRecommend": opRecommend,
    "description": description,
    "ordered": ordered,
    "tags": List<dynamic>.from(tags.map((x) => x)),
    "coverImgId": coverImgId,
    "coverImgUrl": coverImgUrl,
    "newImported": newImported,
    "updateTime": updateTime,
    "specialType": specialType,
    "trackCount": trackCount,
    "commentThreadId": commentThreadId,
    "privacy": privacy,
    "trackUpdateTime": trackUpdateTime,
    "trackNumberUpdateTime": trackNumberUpdateTime,
    "playCount": playCount,
    "subscribedCount": subscribedCount,
    "cloudTrackCount": cloudTrackCount,
    "status": status,
    "userId": userId,
    "createTime": createTime,
    "highQuality": highQuality,
    "adType": adType,
    "name": name,
    "id": id,
    "shareCount": shareCount,
    "coverImgId_str": coverImgIdStr,
    "commentCount": commentCount,
  };
}

class Creator {
  Creator({
    this.defaultAvatar,
    this.province,
    this.authStatus,
    this.followed,
    this.avatarUrl,
    this.accountStatus,
    this.gender,
    this.city,
    this.birthday,
    this.userId,
    this.userType,
    this.nickname,
    this.signature,
    this.description,
    this.detailDescription,
    this.avatarImgId,
    this.backgroundImgId,
    this.backgroundUrl,
    this.authority,
    this.mutual,
    this.expertTags,
    this.experts,
    this.djStatus,
    this.vipType,
    this.remarkName,
    this.authenticationTypes,
    this.avatarDetail,
    this.anchor,
    this.avatarImgIdStr,
    this.backgroundImgIdStr,
    this.creatorAvatarImgIdStr,
  });

  bool defaultAvatar;
  int province;
  int authStatus;
  bool followed;
  String avatarUrl;
  int accountStatus;
  int gender;
  int city;
  int birthday;
  int userId;
  int userType;
  String nickname;
  String signature;
  String description;
  String detailDescription;
  double avatarImgId;
  double backgroundImgId;
  String backgroundUrl;
  int authority;
  bool mutual;
  dynamic expertTags;
  dynamic experts;
  int djStatus;
  int vipType;
  dynamic remarkName;
  int authenticationTypes;
  dynamic avatarDetail;
  bool anchor;
  String avatarImgIdStr;
  String backgroundImgIdStr;
  String creatorAvatarImgIdStr;

  factory Creator.fromJson(Map<String, dynamic> json) => Creator(
    defaultAvatar: json["defaultAvatar"],
    province: json["province"],
    authStatus: json["authStatus"],
    followed: json["followed"],
    avatarUrl: json["avatarUrl"],
    accountStatus: json["accountStatus"],
    gender: json["gender"],
    city: json["city"],
    birthday: json["birthday"],
    userId: json["userId"],
    userType: json["userType"],
    nickname: json["nickname"],
    signature: json["signature"],
    description: json["description"],
    detailDescription: json["detailDescription"],
    avatarImgId: json["avatarImgId"].toDouble(),
    backgroundImgId: json["backgroundImgId"].toDouble(),
    backgroundUrl: json["backgroundUrl"],
    authority: json["authority"],
    mutual: json["mutual"],
    expertTags: json["expertTags"],
    experts: json["experts"],
    djStatus: json["djStatus"],
    vipType: json["vipType"],
    remarkName: json["remarkName"],
    authenticationTypes: json["authenticationTypes"],
    avatarDetail: json["avatarDetail"],
    anchor: json["anchor"],
    avatarImgIdStr: json["avatarImgIdStr"],
    backgroundImgIdStr: json["backgroundImgIdStr"],
    creatorAvatarImgIdStr: json["avatarImgId_str"],
  );

  Map<String, dynamic> toJson() => {
    "defaultAvatar": defaultAvatar,
    "province": province,
    "authStatus": authStatus,
    "followed": followed,
    "avatarUrl": avatarUrl,
    "accountStatus": accountStatus,
    "gender": gender,
    "city": city,
    "birthday": birthday,
    "userId": userId,
    "userType": userType,
    "nickname": nickname,
    "signature": signature,
    "description": description,
    "detailDescription": detailDescription,
    "avatarImgId": avatarImgId,
    "backgroundImgId": backgroundImgId,
    "backgroundUrl": backgroundUrl,
    "authority": authority,
    "mutual": mutual,
    "expertTags": expertTags,
    "experts": experts,
    "djStatus": djStatus,
    "vipType": vipType,
    "remarkName": remarkName,
    "authenticationTypes": authenticationTypes,
    "avatarDetail": avatarDetail,
    "anchor": anchor,
    "avatarImgIdStr": avatarImgIdStr,
    "backgroundImgIdStr": backgroundImgIdStr,
    "avatarImgId_str": creatorAvatarImgIdStr,
  };
}

class TrackId {
  TrackId({
    this.id,
    this.v,
    this.at,
    this.alg,
  });

  int id;
  int v;
  int at;
  dynamic alg;

  factory TrackId.fromJson(Map<String, dynamic> json) => TrackId(
    id: json["id"],
    v: json["v"],
    at: json["at"],
    alg: json["alg"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "v": v,
    "at": at,
    "alg": alg,
  };
}

class Track {
  Track({
    this.name,
    this.id,
    this.pst,
    this.t,
    this.ar,
    this.alia,
    this.pop,
    this.st,
    this.rt,
    this.fee,
    this.v,
    this.crbt,
    this.cf,
    this.al,
    this.dt,
    this.h,
    this.m,
    this.l,
    this.a,
    this.cd,
    this.no,
    this.rtUrl,
    this.ftype,
    this.rtUrls,
    this.djId,
    this.copyright,
    this.sId,
    this.mark,
    this.originCoverType,
    this.noCopyrightRcmd,
    this.rtype,
    this.rurl,
    this.mst,
    this.cp,
    this.mv,
    this.publishTime,
  });

  String name;
  int id;
  int pst;
  int t;
  List<Ar> ar;
  List<String> alia;
  int pop;
  int st;
  dynamic rt;
  int fee;
  int v;
  dynamic crbt;
  String cf;
  Al al;
  int dt;
  H h;
  H m;
  H l;
  dynamic a;
  String cd;
  int no;
  dynamic rtUrl;
  int ftype;
  List<dynamic> rtUrls;
  int djId;
  int copyright;
  int sId;
  int mark;
  int originCoverType;
  NoCopyrightRcmd noCopyrightRcmd;
  int rtype;
  dynamic rurl;
  int mst;
  int cp;
  int mv;
  int publishTime;

  factory Track.fromJson(Map<String, dynamic> json) => Track(
    name: json["name"],
    id: json["id"],
    pst: json["pst"],
    t: json["t"],
    ar: List<Ar>.from(json["ar"].map((x) => Ar.fromJson(x))),
    alia: List<String>.from(json["alia"].map((x) => x)),
    pop: json["pop"],
    st: json["st"],
    rt: json["rt"],
    fee: json["fee"],
    v: json["v"],
    crbt: json["crbt"],
    cf: json["cf"],
    al: Al.fromJson(json["al"]),
    dt: json["dt"],
    h: H.fromJson(json["h"]),
    m: H.fromJson(json["m"]),
    l: json["l"] == null ? null : H.fromJson(json["l"]),
    a: json["a"],
    cd: json["cd"],
    no: json["no"],
    rtUrl: json["rtUrl"],
    ftype: json["ftype"],
    rtUrls: List<dynamic>.from(json["rtUrls"].map((x) => x)),
    djId: json["djId"],
    copyright: json["copyright"],
    sId: json["s_id"],
    mark: json["mark"],
    originCoverType: json["originCoverType"],
    noCopyrightRcmd: json["noCopyrightRcmd"] == null ? null : NoCopyrightRcmd.fromJson(json["noCopyrightRcmd"]),
    rtype: json["rtype"],
    rurl: json["rurl"],
    mst: json["mst"],
    cp: json["cp"],
    mv: json["mv"],
    publishTime: json["publishTime"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "id": id,
    "pst": pst,
    "t": t,
    "ar": List<dynamic>.from(ar.map((x) => x.toJson())),
    "alia": List<dynamic>.from(alia.map((x) => x)),
    "pop": pop,
    "st": st,
    "rt": rt,
    "fee": fee,
    "v": v,
    "crbt": crbt,
    "cf": cf,
    "al": al.toJson(),
    "dt": dt,
    "h": h.toJson(),
    "m": m.toJson(),
    "l": l == null ? null : l.toJson(),
    "a": a,
    "cd": cd,
    "no": no,
    "rtUrl": rtUrl,
    "ftype": ftype,
    "rtUrls": List<dynamic>.from(rtUrls.map((x) => x)),
    "djId": djId,
    "copyright": copyright,
    "s_id": sId,
    "mark": mark,
    "originCoverType": originCoverType,
    "noCopyrightRcmd": noCopyrightRcmd == null ? null : noCopyrightRcmd.toJson(),
    "rtype": rtype,
    "rurl": rurl,
    "mst": mst,
    "cp": cp,
    "mv": mv,
    "publishTime": publishTime,
  };
}

class Al {
  Al({
    this.id,
    this.name,
    this.picUrl,
    this.tns,
    this.picStr,
    this.pic,
  });

  int id;
  String name;
  String picUrl;
  List<dynamic> tns;
  String picStr;
  double pic;

  factory Al.fromJson(Map<String, dynamic> json) => Al(
    id: json["id"],
    name: json["name"],
    picUrl: json["picUrl"],
    tns: List<dynamic>.from(json["tns"].map((x) => x)),
    picStr: json["pic_str"] == null ? null : json["pic_str"],
    pic: json["pic"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "picUrl": picUrl,
    "tns": List<dynamic>.from(tns.map((x) => x)),
    "pic_str": picStr == null ? null : picStr,
    "pic": pic,
  };
}

class Ar {
  Ar({
    this.id,
    this.name,
    this.tns,
    this.alias,
  });

  int id;
  Name name;
  List<dynamic> tns;
  List<dynamic> alias;

  factory Ar.fromJson(Map<String, dynamic> json) => Ar(
    id: json["id"],
    name: nameValues.map[json["name"]],
    tns: List<dynamic>.from(json["tns"].map((x) => x)),
    alias: List<dynamic>.from(json["alias"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": nameValues.reverse[name],
    "tns": List<dynamic>.from(tns.map((x) => x)),
    "alias": List<dynamic>.from(alias.map((x) => x)),
  };
}

enum Name { EMPTY, NZBZ, NAME }

final nameValues = EnumValues({
  "高铭": Name.EMPTY,
  "黄龄": Name.NAME,
  "南征北战NZBZ": Name.NZBZ
});

class H {
  H({
    this.br,
    this.fid,
    this.size,
    this.vd,
  });

  int br;
  int fid;
  int size;
  int vd;

  factory H.fromJson(Map<String, dynamic> json) => H(
    br: json["br"],
    fid: json["fid"],
    size: json["size"],
    vd: json["vd"],
  );

  Map<String, dynamic> toJson() => {
    "br": br,
    "fid": fid,
    "size": size,
    "vd": vd,
  };
}

class NoCopyrightRcmd {
  NoCopyrightRcmd({
    this.type,
    this.typeDesc,
    this.songId,
  });

  int type;
  String typeDesc;
  dynamic songId;

  factory NoCopyrightRcmd.fromJson(Map<String, dynamic> json) => NoCopyrightRcmd(
    type: json["type"],
    typeDesc: json["typeDesc"],
    songId: json["songId"],
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "typeDesc": typeDesc,
    "songId": songId,
  };
}

class Privilege {
  Privilege({
    this.id,
    this.fee,
    this.payed,
    this.st,
    this.pl,
    this.dl,
    this.sp,
    this.cp,
    this.subp,
    this.cs,
    this.maxbr,
    this.fl,
    this.toast,
    this.flag,
    this.preSell,
    this.playMaxbr,
    this.downloadMaxbr,
    this.freeTrialPrivilege,
    this.chargeInfoList,
  });

  int id;
  int fee;
  int payed;
  int st;
  int pl;
  int dl;
  int sp;
  int cp;
  int subp;
  bool cs;
  int maxbr;
  int fl;
  bool toast;
  int flag;
  bool preSell;
  int playMaxbr;
  int downloadMaxbr;
  FreeTrialPrivilege freeTrialPrivilege;
  List<ChargeInfoList> chargeInfoList;

  factory Privilege.fromJson(Map<String, dynamic> json) => Privilege(
    id: json["id"],
    fee: json["fee"],
    payed: json["payed"],
    st: json["st"],
    pl: json["pl"],
    dl: json["dl"],
    sp: json["sp"],
    cp: json["cp"],
    subp: json["subp"],
    cs: json["cs"],
    maxbr: json["maxbr"],
    fl: json["fl"],
    toast: json["toast"],
    flag: json["flag"],
    preSell: json["preSell"],
    playMaxbr: json["playMaxbr"],
    downloadMaxbr: json["downloadMaxbr"],
    freeTrialPrivilege: FreeTrialPrivilege.fromJson(json["freeTrialPrivilege"]),
    chargeInfoList: List<ChargeInfoList>.from(json["chargeInfoList"].map((x) => ChargeInfoList.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fee": fee,
    "payed": payed,
    "st": st,
    "pl": pl,
    "dl": dl,
    "sp": sp,
    "cp": cp,
    "subp": subp,
    "cs": cs,
    "maxbr": maxbr,
    "fl": fl,
    "toast": toast,
    "flag": flag,
    "preSell": preSell,
    "playMaxbr": playMaxbr,
    "downloadMaxbr": downloadMaxbr,
    "freeTrialPrivilege": freeTrialPrivilege.toJson(),
    "chargeInfoList": List<dynamic>.from(chargeInfoList.map((x) => x.toJson())),
  };
}

class ChargeInfoList {
  ChargeInfoList({
    this.rate,
    this.chargeUrl,
    this.chargeMessage,
    this.chargeType,
  });

  int rate;
  dynamic chargeUrl;
  dynamic chargeMessage;
  int chargeType;

  factory ChargeInfoList.fromJson(Map<String, dynamic> json) => ChargeInfoList(
    rate: json["rate"],
    chargeUrl: json["chargeUrl"],
    chargeMessage: json["chargeMessage"],
    chargeType: json["chargeType"],
  );

  Map<String, dynamic> toJson() => {
    "rate": rate,
    "chargeUrl": chargeUrl,
    "chargeMessage": chargeMessage,
    "chargeType": chargeType,
  };
}

class FreeTrialPrivilege {
  FreeTrialPrivilege({
    this.resConsumable,
    this.userConsumable,
  });

  bool resConsumable;
  bool userConsumable;

  factory FreeTrialPrivilege.fromJson(Map<String, dynamic> json) => FreeTrialPrivilege(
    resConsumable: json["resConsumable"],
    userConsumable: json["userConsumable"],
  );

  Map<String, dynamic> toJson() => {
    "resConsumable": resConsumable,
    "userConsumable": userConsumable,
  };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
