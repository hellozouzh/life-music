// To parse this JSON data, do
//
//     final musicPlayingList = musicPlayingListFromJson(jsonString);

import 'dart:convert';

MusicPlayingList musicPlayingListFromJson(String str) => MusicPlayingList.fromJson(json.decode(str));

String musicPlayingListToJson(MusicPlayingList data) => json.encode(data.toJson());

class MusicPlayingList {
  MusicPlayingList({
    this.songs,
  });

  List<Song> songs;

  factory MusicPlayingList.fromJson(Map<String, dynamic> json) => MusicPlayingList(
    songs: List<Song>.from(json["songs"].map((x) => Song.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "songs": List<dynamic>.from(songs.map((x) => x.toJson())),
  };
}

class Song {
  Song({
    this.name,
    this.id,
    this.pst,
    this.t,
    this.ar,
    this.alia,
    this.pop,
    this.st,
    this.rt,
    this.fee,
    this.v,
    this.crbt,
    this.cf,
    this.al,
    this.dt,
    this.h,
    this.m,
    this.l,
    this.a,
    this.cd,
    this.no,
    this.rtUrl,
    this.ftype,
    this.rtUrls,
    this.djId,
    this.copyright,
    this.sId,
    this.mark,
    this.originCoverType,
    this.single,
    this.noCopyrightRcmd,
    this.mv,
    this.mst,
    this.cp,
    this.rtype,
    this.rurl,
    this.publishTime,
  });

  String name;
  int id;
  int pst;
  int t;
  List<Ar> ar;
  List<String> alia;
  int pop;
  int st;
  String rt;
  int fee;
  int v;
  dynamic crbt;
  String cf;
  Al al;
  int dt;
  L h;
  L m;
  L l;
  dynamic a;
  String cd;
  int no;
  dynamic rtUrl;
  int ftype;
  List<dynamic> rtUrls;
  int djId;
  int copyright;
  int sId;
  double mark;
  int originCoverType;
  int single;
  NoCopyrightRcmd noCopyrightRcmd;
  int mv;
  int mst;
  int cp;
  int rtype;
  dynamic rurl;
  int publishTime;

  factory Song.fromJson(Map<String, dynamic> json) => Song(
    name: json["name"],
    id: json["id"],
    pst: json["pst"],
    t: json["t"],
    ar: List<Ar>.from(json["ar"].map((x) => Ar.fromJson(x))),
    alia: List<String>.from(json["alia"].map((x) => x)),
    pop: json["pop"],
    st: json["st"],
    rt: json["rt"] == null ? null : json["rt"],
    fee: json["fee"],
    v: json["v"],
    crbt: json["crbt"],
    cf: json["cf"],
    al: Al.fromJson(json["al"]),
    dt: json["dt"],
    h: json["h"] == null ? null : L.fromJson(json["h"]),
    m: json["m"] == null ? null : L.fromJson(json["m"]),
    l: L.fromJson(json["l"]),
    a: json["a"],
    cd: json["cd"],
    no: json["no"],
    rtUrl: json["rtUrl"],
    ftype: json["ftype"],
    rtUrls: List<dynamic>.from(json["rtUrls"].map((x) => x)),
    djId: json["djId"],
    copyright: json["copyright"],
    sId: json["s_id"],
    mark: json["mark"].toDouble(),
    originCoverType: json["originCoverType"],
    single: json["single"],
    noCopyrightRcmd: json["noCopyrightRcmd"] == null ? null : NoCopyrightRcmd.fromJson(json["noCopyrightRcmd"]),
    mv: json["mv"],
    mst: json["mst"],
    cp: json["cp"],
    rtype: json["rtype"],
    rurl: json["rurl"],
    publishTime: json["publishTime"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "id": id,
    "pst": pst,
    "t": t,
    "ar": List<dynamic>.from(ar.map((x) => x.toJson())),
    "alia": List<dynamic>.from(alia.map((x) => x)),
    "pop": pop,
    "st": st,
    "rt": rt == null ? null : rt,
    "fee": fee,
    "v": v,
    "crbt": crbt,
    "cf": cf,
    "al": al.toJson(),
    "dt": dt,
    "h": h == null ? null : h.toJson(),
    "m": m == null ? null : m.toJson(),
    "l": l.toJson(),
    "a": a,
    "cd": cd,
    "no": no,
    "rtUrl": rtUrl,
    "ftype": ftype,
    "rtUrls": List<dynamic>.from(rtUrls.map((x) => x)),
    "djId": djId,
    "copyright": copyright,
    "s_id": sId,
    "mark": mark,
    "originCoverType": originCoverType,
    "single": single,
    "noCopyrightRcmd": noCopyrightRcmd == null ? null : noCopyrightRcmd.toJson(),
    "mv": mv,
    "mst": mst,
    "cp": cp,
    "rtype": rtype,
    "rurl": rurl,
    "publishTime": publishTime,
  };
}

class Al {
  Al({
    this.id,
    this.name,
    this.picUrl,
    this.tns,
    this.picStr,
    this.pic,
  });

  int id;
  String name;
  String picUrl;
  List<dynamic> tns;
  String picStr;
  double pic;

  factory Al.fromJson(Map<String, dynamic> json) => Al(
    id: json["id"],
    name: json["name"],
    picUrl: json["picUrl"],
    tns: List<dynamic>.from(json["tns"].map((x) => x)),
    picStr: json["pic_str"] == null ? null : json["pic_str"],
    pic: json["pic"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "picUrl": picUrl,
    "tns": List<dynamic>.from(tns.map((x) => x)),
    "pic_str": picStr == null ? null : picStr,
    "pic": pic,
  };
}

class Ar {
  Ar({
    this.id,
    this.name,
    this.tns,
    this.alias,
  });

  int id;
  String name;
  List<dynamic> tns;
  List<dynamic> alias;

  factory Ar.fromJson(Map<String, dynamic> json) => Ar(
    id: json["id"],
    name: json["name"],
    tns: List<dynamic>.from(json["tns"].map((x) => x)),
    alias: List<dynamic>.from(json["alias"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "tns": List<dynamic>.from(tns.map((x) => x)),
    "alias": List<dynamic>.from(alias.map((x) => x)),
  };
}

class L {
  L({
    this.br,
    this.fid,
    this.size,
    this.vd,
  });

  int br;
  int fid;
  int size;
  int vd;

  factory L.fromJson(Map<String, dynamic> json) => L(
    br: json["br"],
    fid: json["fid"],
    size: json["size"],
    vd: json["vd"],
  );

  Map<String, dynamic> toJson() => {
    "br": br,
    "fid": fid,
    "size": size,
    "vd": vd,
  };
}

class NoCopyrightRcmd {
  NoCopyrightRcmd({
    this.type,
    this.typeDesc,
    this.songId,
  });

  int type;
  String typeDesc;
  dynamic songId;

  factory NoCopyrightRcmd.fromJson(Map<String, dynamic> json) => NoCopyrightRcmd(
    type: json["type"],
    typeDesc: json["typeDesc"],
    songId: json["songId"],
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "typeDesc": typeDesc,
    "songId": songId,
  };
}
