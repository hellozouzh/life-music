// To parse this JSON data, do
//
//     final remakeModel = remakeModelFromJson(jsonString);

import 'dart:convert';

RemakeModel remakeModelFromJson(String str) => RemakeModel.fromJson(json.decode(str));

String remakeModelToJson(RemakeModel data) => json.encode(data.toJson());

class RemakeModel {
  RemakeModel({
    this.isMusician,
    this.userId,
    this.topComments,
    this.moreHot,
    this.hotComments,
    this.commentBanner,
    this.code,
    this.comments,
    this.total,
    this.more,
  });

  bool isMusician;
  int userId;
  List<dynamic> topComments;
  bool moreHot;
  List<Comment> hotComments;
  dynamic commentBanner;
  int code;
  List<Comment> comments;
  int total;
  bool more;

  factory RemakeModel.fromJson(Map<String, dynamic> json) => RemakeModel(
    isMusician: json["isMusician"],
    userId: json["userId"],
    topComments: List<dynamic>.from(json["topComments"].map((x) => x)),
    moreHot: json["moreHot"],
    hotComments: List<Comment>.from(json["hotComments"].map((x) => Comment.fromJson(x))),
    commentBanner: json["commentBanner"],
    code: json["code"],
    comments: List<Comment>.from(json["comments"].map((x) => Comment.fromJson(x))),
    total: json["total"],
    more: json["more"],
  );

  Map<String, dynamic> toJson() => {
    "isMusician": isMusician,
    "userId": userId,
    "topComments": List<dynamic>.from(topComments.map((x) => x)),
    "moreHot": moreHot,
    "hotComments": List<dynamic>.from(hotComments.map((x) => x.toJson())),
    "commentBanner": commentBanner,
    "code": code,
    "comments": List<dynamic>.from(comments.map((x) => x.toJson())),
    "total": total,
    "more": more,
  };
}

class Comment {
  Comment({
    this.user,
    this.beReplied,
    this.pendantData,
    this.showFloorComment,
    this.status,
    this.commentId,
    this.content,
    this.time,
    this.likedCount,
    this.expressionUrl,
    this.commentLocationType,
    this.parentCommentId,
    this.decoration,
    this.repliedMark,
    this.liked,
  });

  User user;
  List<BeReplied> beReplied;
  PendantData pendantData;
  dynamic showFloorComment;
  int status;
  int commentId;
  String content;
  int time;
  int likedCount;
  dynamic expressionUrl;
  int commentLocationType;
  int parentCommentId;
  Decoration decoration;
  dynamic repliedMark;
  bool liked;

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
    user: User.fromJson(json["user"]),
    beReplied: List<BeReplied>.from(json["beReplied"].map((x) => BeReplied.fromJson(x))),
    pendantData: json["pendantData"] == null ? null : PendantData.fromJson(json["pendantData"]),
    showFloorComment: json["showFloorComment"],
    status: json["status"],
    commentId: json["commentId"],
    content: json["content"],
    time: json["time"],
    likedCount: json["likedCount"],
    expressionUrl: json["expressionUrl"],
    commentLocationType: json["commentLocationType"],
    parentCommentId: json["parentCommentId"],
    decoration: json["decoration"] == null ? null : Decoration.fromJson(json["decoration"]),
    repliedMark: json["repliedMark"],
    liked: json["liked"],
  );

  Map<String, dynamic> toJson() => {
    "user": user.toJson(),
    "beReplied": List<dynamic>.from(beReplied.map((x) => x.toJson())),
    "pendantData": pendantData == null ? null : pendantData.toJson(),
    "showFloorComment": showFloorComment,
    "status": status,
    "commentId": commentId,
    "content": content,
    "time": time,
    "likedCount": likedCount,
    "expressionUrl": expressionUrl,
    "commentLocationType": commentLocationType,
    "parentCommentId": parentCommentId,
    "decoration": decoration == null ? null : decoration.toJson(),
    "repliedMark": repliedMark,
    "liked": liked,
  };
}

class BeReplied {
  BeReplied({
    this.user,
    this.beRepliedCommentId,
    this.content,
    this.status,
    this.expressionUrl,
  });

  User user;
  int beRepliedCommentId;
  String content;
  int status;
  dynamic expressionUrl;

  factory BeReplied.fromJson(Map<String, dynamic> json) => BeReplied(
    user: User.fromJson(json["user"]),
    beRepliedCommentId: json["beRepliedCommentId"],
    content: json["content"],
    status: json["status"],
    expressionUrl: json["expressionUrl"],
  );

  Map<String, dynamic> toJson() => {
    "user": user.toJson(),
    "beRepliedCommentId": beRepliedCommentId,
    "content": content,
    "status": status,
    "expressionUrl": expressionUrl,
  };
}

class User {
  User({
    this.locationInfo,
    this.liveInfo,
    this.anonym,
    this.remarkName,
    this.vipRights,
    this.nickname,
    this.avatarUrl,
    this.authStatus,
    this.expertTags,
    this.experts,
    this.vipType,
    this.userId,
    this.avatarDetail,
    this.userType,
  });

  dynamic locationInfo;
  dynamic liveInfo;
  int anonym;
  dynamic remarkName;
  VipRights vipRights;
  String nickname;
  String avatarUrl;
  int authStatus;
  dynamic expertTags;
  dynamic experts;
  int vipType;
  int userId;
  dynamic avatarDetail;
  int userType;

  factory User.fromJson(Map<String, dynamic> json) => User(
    locationInfo: json["locationInfo"],
    liveInfo: json["liveInfo"],
    anonym: json["anonym"],
    remarkName: json["remarkName"],
    vipRights: json["vipRights"] == null ? null : VipRights.fromJson(json["vipRights"]),
    nickname: json["nickname"],
    avatarUrl: json["avatarUrl"],
    authStatus: json["authStatus"],
    expertTags: json["expertTags"],
    experts: json["experts"],
    vipType: json["vipType"],
    userId: json["userId"],
    avatarDetail: json["avatarDetail"],
    userType: json["userType"],
  );

  Map<String, dynamic> toJson() => {
    "locationInfo": locationInfo,
    "liveInfo": liveInfo,
    "anonym": anonym,
    "remarkName": remarkName,
    "vipRights": vipRights == null ? null : vipRights.toJson(),
    "nickname": nickname,
    "avatarUrl": avatarUrl,
    "authStatus": authStatus,
    "expertTags": expertTags,
    "experts": experts,
    "vipType": vipType,
    "userId": userId,
    "avatarDetail": avatarDetail,
    "userType": userType,
  };
}

class VipRights {
  VipRights({
    this.associator,
    this.musicPackage,
    this.redVipAnnualCount,
    this.redVipLevel,
  });

  Associator associator;
  Associator musicPackage;
  int redVipAnnualCount;
  int redVipLevel;

  factory VipRights.fromJson(Map<String, dynamic> json) => VipRights(
    associator: json["associator"] == null ? null : Associator.fromJson(json["associator"]),
    musicPackage: json["musicPackage"] == null ? null : Associator.fromJson(json["musicPackage"]),
    redVipAnnualCount: json["redVipAnnualCount"],
    redVipLevel: json["redVipLevel"],
  );

  Map<String, dynamic> toJson() => {
    "associator": associator == null ? null : associator.toJson(),
    "musicPackage": musicPackage == null ? null : musicPackage.toJson(),
    "redVipAnnualCount": redVipAnnualCount,
    "redVipLevel": redVipLevel,
  };
}

class Associator {
  Associator({
    this.vipCode,
    this.rights,
  });

  int vipCode;
  bool rights;

  factory Associator.fromJson(Map<String, dynamic> json) => Associator(
    vipCode: json["vipCode"],
    rights: json["rights"],
  );

  Map<String, dynamic> toJson() => {
    "vipCode": vipCode,
    "rights": rights,
  };
}

class Decoration {
  Decoration();

  factory Decoration.fromJson(Map<String, dynamic> json) => Decoration(
  );

  Map<String, dynamic> toJson() => {
  };
}

class PendantData {
  PendantData({
    this.id,
    this.imageUrl,
  });

  int id;
  String imageUrl;

  factory PendantData.fromJson(Map<String, dynamic> json) => PendantData(
    id: json["id"],
    imageUrl: json["imageUrl"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "imageUrl": imageUrl,
  };
}
