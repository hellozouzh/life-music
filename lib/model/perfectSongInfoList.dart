// To parse this JSON data, do
//
//     final perfectSongList = perfectSongListFromJson(jsonString);

import 'dart:convert';

PerfectSongList perfectSongListFromJson(String str) => PerfectSongList.fromJson(json.decode(str));

String perfectSongListToJson(PerfectSongList data) => json.encode(data.toJson());

class PerfectSongList {
  PerfectSongList({
    this.songs,
    this.privileges,
    this.code,
  });

  List<Song> songs;
  List<Privilege> privileges;
  int code;

  factory PerfectSongList.fromJson(Map<String, dynamic> json) => PerfectSongList(
    songs: List<Song>.from(json["songs"].map((x) => Song.fromJson(x))),
    privileges: List<Privilege>.from(json["privileges"].map((x) => Privilege.fromJson(x))),
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "songs": List<dynamic>.from(songs.map((x) => x.toJson())),
    "privileges": List<dynamic>.from(privileges.map((x) => x.toJson())),
    "code": code,
  };
}

class Privilege {
  Privilege({
    this.id,
    this.fee,
    this.payed,
    this.st,
    this.pl,
    this.dl,
    this.sp,
    this.cp,
    this.subp,
    this.cs,
    this.maxbr,
    this.fl,
    this.toast,
    this.flag,
    this.preSell,
    this.playMaxbr,
    this.downloadMaxbr,
    this.freeTrialPrivilege,
    this.chargeInfoList,
  });

  int id;
  int fee;
  int payed;
  int st;
  int pl;
  int dl;
  int sp;
  int cp;
  int subp;
  bool cs;
  int maxbr;
  int fl;
  bool toast;
  int flag;
  bool preSell;
  int playMaxbr;
  int downloadMaxbr;
  FreeTrialPrivilege freeTrialPrivilege;
  List<ChargeInfoList> chargeInfoList;

  factory Privilege.fromJson(Map<String, dynamic> json) => Privilege(
    id: json["id"],
    fee: json["fee"],
    payed: json["payed"],
    st: json["st"],
    pl: json["pl"],
    dl: json["dl"],
    sp: json["sp"],
    cp: json["cp"],
    subp: json["subp"],
    cs: json["cs"],
    maxbr: json["maxbr"],
    fl: json["fl"],
    toast: json["toast"],
    flag: json["flag"],
    preSell: json["preSell"],
    playMaxbr: json["playMaxbr"],
    downloadMaxbr: json["downloadMaxbr"],
    freeTrialPrivilege: FreeTrialPrivilege.fromJson(json["freeTrialPrivilege"]),
    chargeInfoList: List<ChargeInfoList>.from(json["chargeInfoList"].map((x) => ChargeInfoList.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fee": fee,
    "payed": payed,
    "st": st,
    "pl": pl,
    "dl": dl,
    "sp": sp,
    "cp": cp,
    "subp": subp,
    "cs": cs,
    "maxbr": maxbr,
    "fl": fl,
    "toast": toast,
    "flag": flag,
    "preSell": preSell,
    "playMaxbr": playMaxbr,
    "downloadMaxbr": downloadMaxbr,
    "freeTrialPrivilege": freeTrialPrivilege.toJson(),
    "chargeInfoList": List<dynamic>.from(chargeInfoList.map((x) => x.toJson())),
  };
}

class ChargeInfoList {
  ChargeInfoList({
    this.rate,
    this.chargeUrl,
    this.chargeMessage,
    this.chargeType,
  });

  int rate;
  dynamic chargeUrl;
  dynamic chargeMessage;
  int chargeType;

  factory ChargeInfoList.fromJson(Map<String, dynamic> json) => ChargeInfoList(
    rate: json["rate"],
    chargeUrl: json["chargeUrl"],
    chargeMessage: json["chargeMessage"],
    chargeType: json["chargeType"],
  );

  Map<String, dynamic> toJson() => {
    "rate": rate,
    "chargeUrl": chargeUrl,
    "chargeMessage": chargeMessage,
    "chargeType": chargeType,
  };
}

class FreeTrialPrivilege {
  FreeTrialPrivilege({
    this.resConsumable,
    this.userConsumable,
  });

  bool resConsumable;
  bool userConsumable;

  factory FreeTrialPrivilege.fromJson(Map<String, dynamic> json) => FreeTrialPrivilege(
    resConsumable: json["resConsumable"],
    userConsumable: json["userConsumable"],
  );

  Map<String, dynamic> toJson() => {
    "resConsumable": resConsumable,
    "userConsumable": userConsumable,
  };
}

class Song {
  Song({
    this.name,
    this.id,
    this.pst,
    this.t,
    this.ar,
    this.alia,
    this.pop,
    this.st,
    this.rt,
    this.fee,
    this.v,
    this.crbt,
    this.cf,
    this.al,
    this.dt,
    this.h,
    this.m,
    this.l,
    this.a,
    this.cd,
    this.no,
    this.rtUrl,
    this.ftype,
    this.rtUrls,
    this.djId,
    this.copyright,
    this.sId,
    this.mark,
    this.originCoverType,
    this.single,
    this.noCopyrightRcmd,
    this.mv,
    this.mst,
    this.cp,
    this.rtype,
    this.rurl,
    this.publishTime,
    this.tns,
  });

  String name;
  int id;
  int pst;
  int t;
  List<Ar> ar;
  List<String> alia;
  int pop;
  int st;
  String rt;
  int fee;
  int v;
  dynamic crbt;
  String cf;
  Al al;
  int dt;
  H h;
  H m;
  H l;
  dynamic a;
  String cd;
  int no;
  dynamic rtUrl;
  int ftype;
  List<dynamic> rtUrls;
  int djId;
  int copyright;
  int sId;
  int mark;
  int originCoverType;
  int single;
  NoCopyrightRcmd noCopyrightRcmd;
  int mv;
  int mst;
  int cp;
  int rtype;
  dynamic rurl;
  int publishTime;
  List<String> tns;

  factory Song.fromJson(Map<String, dynamic> json) => Song(
    name: json["name"],
    id: json["id"],
    pst: json["pst"],
    t: json["t"],
    ar: List<Ar>.from(json["ar"].map((x) => Ar.fromJson(x))),
    alia: List<String>.from(json["alia"].map((x) => x)),
    pop: json["pop"],
    st: json["st"],
    rt: json["rt"] == null ? null : json["rt"],
    fee: json["fee"],
    v: json["v"],
    crbt: json["crbt"],
    cf: json["cf"],
    al: Al.fromJson(json["al"]),
    dt: json["dt"],
    h: H.fromJson(json["h"]),
    m: H.fromJson(json["m"]),
    l: H.fromJson(json["l"]),
    a: json["a"],
    cd: json["cd"],
    no: json["no"],
    rtUrl: json["rtUrl"],
    ftype: json["ftype"],
    rtUrls: List<dynamic>.from(json["rtUrls"].map((x) => x)),
    djId: json["djId"],
    copyright: json["copyright"],
    sId: json["s_id"],
    mark: json["mark"],
    originCoverType: json["originCoverType"],
    single: json["single"],
    noCopyrightRcmd: json["noCopyrightRcmd"] == null ? null : NoCopyrightRcmd.fromJson(json["noCopyrightRcmd"]),
    mv: json["mv"],
    mst: json["mst"],
    cp: json["cp"],
    rtype: json["rtype"],
    rurl: json["rurl"],
    publishTime: json["publishTime"],
    tns: json["tns"] == null ? null : List<String>.from(json["tns"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "id": id,
    "pst": pst,
    "t": t,
    "ar": List<dynamic>.from(ar.map((x) => x.toJson())),
    "alia": List<dynamic>.from(alia.map((x) => x)),
    "pop": pop,
    "st": st,
    "rt": rt == null ? null : rt,
    "fee": fee,
    "v": v,
    "crbt": crbt,
    "cf": cf,
    "al": al.toJson(),
    "dt": dt,
    "h": h.toJson(),
    "m": m.toJson(),
    "l": l.toJson(),
    "a": a,
    "cd": cd,
    "no": no,
    "rtUrl": rtUrl,
    "ftype": ftype,
    "rtUrls": List<dynamic>.from(rtUrls.map((x) => x)),
    "djId": djId,
    "copyright": copyright,
    "s_id": sId,
    "mark": mark,
    "originCoverType": originCoverType,
    "single": single,
    "noCopyrightRcmd": noCopyrightRcmd == null ? null : noCopyrightRcmd.toJson(),
    "mv": mv,
    "mst": mst,
    "cp": cp,
    "rtype": rtype,
    "rurl": rurl,
    "publishTime": publishTime,
    "tns": tns == null ? null : List<dynamic>.from(tns.map((x) => x)),
  };
}

class Al {
  Al({
    this.id,
    this.name,
    this.picUrl,
    this.tns,
    this.pic,
    this.picStr,
  });

  int id;
  String name;
  String picUrl;
  List<dynamic> tns;
  double pic;
  String picStr;

  factory Al.fromJson(Map<String, dynamic> json) => Al(
    id: json["id"],
    name: json["name"],
    picUrl: json["picUrl"],
    tns: List<dynamic>.from(json["tns"].map((x) => x)),
    pic: json["pic"].toDouble(),
    picStr: json["pic_str"] == null ? null : json["pic_str"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "picUrl": picUrl,
    "tns": List<dynamic>.from(tns.map((x) => x)),
    "pic": pic,
    "pic_str": picStr == null ? null : picStr,
  };
}

class Ar {
  Ar({
    this.id,
    this.name,
    this.tns,
    this.alias,
  });

  int id;
  String name;
  List<dynamic> tns;
  List<dynamic> alias;

  factory Ar.fromJson(Map<String, dynamic> json) => Ar(
    id: json["id"],
    name: json["name"],
    tns: List<dynamic>.from(json["tns"].map((x) => x)),
    alias: List<dynamic>.from(json["alias"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "tns": List<dynamic>.from(tns.map((x) => x)),
    "alias": List<dynamic>.from(alias.map((x) => x)),
  };
}

class H {
  H({
    this.br,
    this.fid,
    this.size,
    this.vd,
  });

  int br;
  int fid;
  int size;
  int vd;

  factory H.fromJson(Map<String, dynamic> json) => H(
    br: json["br"],
    fid: json["fid"],
    size: json["size"],
    vd: json["vd"],
  );

  Map<String, dynamic> toJson() => {
    "br": br,
    "fid": fid,
    "size": size,
    "vd": vd,
  };
}

class NoCopyrightRcmd {
  NoCopyrightRcmd({
    this.type,
    this.typeDesc,
    this.songId,
  });

  int type;
  String typeDesc;
  String songId;

  factory NoCopyrightRcmd.fromJson(Map<String, dynamic> json) => NoCopyrightRcmd(
    type: json["type"],
    typeDesc: json["typeDesc"],
    songId: json["songId"] == null ? null : json["songId"],
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "typeDesc": typeDesc,
    "songId": songId == null ? null : songId,
  };
}
