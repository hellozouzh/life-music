import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';
import 'package:LifeMusic/routers/application.dart';

import 'package:flutter/rendering.dart';

import 'package:flutter/widgets.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:LifeMusic/pages/firstPages/playListPage.dart';
import 'package:LifeMusic/pages/fourthPages/minePage.dart';
import 'package:LifeMusic/pages/secondPages/favoritePage.dart';
import 'package:LifeMusic/pages/thirdPages/searchPage.dart';

import 'package:provider/provider.dart';
import 'package:LifeMusic/pages/secondPages/userDrawer.dart';
import 'package:LifeMusic/provider/musicPlayingProvider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

//继承SingleTickerProviderStateMixin，提供单个Ticker（每个动画帧调用它的回调一次）
class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  int _currentIndex = 1;
  var _pageController = new PageController(initialPage: 1);

  TabController _tabController;
  DateTime _lastPressedAt; //上次点击时间

  List currentPages = [
    PlayListPage(),
    FavoritePage(),
    SearchPage(),
    MinePage()
  ];

  void _pageChange(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  void onBottomNavTap(int index) {
    setState(() {
      _currentIndex = index;
    });
    _pageController.animateToPage(index,
        duration: const Duration(milliseconds: 300), curve: Curves.ease);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (_lastPressedAt == null ||
            DateTime.now().difference(_lastPressedAt) > Duration(seconds: 1)) {
          //两次点击间隔超过1秒则重新计时
          _lastPressedAt = DateTime.now();
          EasyLoading.showToast('再按一次退出应用',
              toastPosition: EasyLoadingToastPosition.bottom);
          return false;
        }
        return true;
      },
      child: Scaffold(
          bottomNavigationBar: Container(
            child: ClipRRect(
                // borderRadius: BorderRadius.only(topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0), ),
                child: BottomNavigationBar(
                    unselectedItemColor: Colors.white,
                    selectedItemColor: Color(0xFFB820FF),
                    backgroundColor: Color(0xff1d1726),
                    items: [
                      BottomNavigationBarItem(
                        icon: Icon(Icons.my_library_music_sharp),
                        title: Text('列表'),
                      ),
                      BottomNavigationBarItem(
                        icon: Icon(Icons.favorite),
                        title: Text('最爱'),
                      ),
                      BottomNavigationBarItem(
                        icon: Icon(Icons.search),
                        title: Text('发现'),
                      ),
                      BottomNavigationBarItem(
                        icon: Icon(Icons.people_alt),
                        title: Text('我的'),
                      ),
                    ],
                    type: BottomNavigationBarType.fixed,
                    currentIndex: _currentIndex,
                    onTap: onBottomNavTap)),
          ),
          body: PageView.builder(
            itemCount: 4,
            controller: _pageController,
            onPageChanged: _pageChange,
            itemBuilder: (context, index) {
              return currentPages[index];
            },
          )),
    );
  }
}
