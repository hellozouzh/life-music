import 'package:LifeMusic/routers/application.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class LoginTypePage extends StatefulWidget {
  @override
  _LoginTypePageState createState() => _LoginTypePageState();
}

class _LoginTypePageState extends State<LoginTypePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff1d1726),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'WELCOME!',
              style: TextStyle(color: Colors.white, fontSize: 26),
            ),
            radiusBox('手机号/邮箱登录',callBack: (){
              Application.router.navigateTo(context, '/loginPage?type=phone',transition: TransitionType.fadeIn);
            }),
            radiusBox('立即注册',callBack:(){
              Application.router.navigateTo(context, '/registerPage',transition: TransitionType.fadeIn);
            }),
            // radiusBox( '邮箱登录',callBack:(){
            //   Application.router.navigateTo(context, '/loginPage?type=email',transition: TransitionType.fadeIn);
            // }),
          ],
        ),
      ),
    );
  }

  Widget radiusBox(String title ,{Function callBack}) {
    return InkWell(
      onTap: (){
        callBack();
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(color: Color(0xFFB820FF), width: 1.5),
            borderRadius: new BorderRadius.circular((20.0))),
        width: MediaQuery.of(context).size.width / 1.5,
        height: 40,
        margin: EdgeInsets.all(20.0),
        child: Center(
          child: Text(
            title,
            style: TextStyle(
                color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600),
          ),
        ),
      ),
    );
  }
}
