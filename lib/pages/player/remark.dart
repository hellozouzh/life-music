import 'package:flutter/material.dart';
import 'package:LifeMusic/api/songListApi.dart';
import 'package:flutter_vant_kit/widgets/loading.dart';
import 'package:like_button/like_button.dart';

import 'package:provider/provider.dart';
import 'package:LifeMusic/provider/musicPlayingProvider.dart';

import 'package:LifeMusic/model/musicInfoModel.dart';
import 'package:LifeMusic/model/remakeModel.dart';

class RemakeList extends StatefulWidget {
  @override
  _RemakeListState createState() => _RemakeListState();
}

class _RemakeListState extends State<RemakeList> {
  int offset = 1;
  MusicInfoModel musicDetail = MusicInfoModel();
  RemakeModel remakeModel = RemakeModel();
  Map<String, dynamic> songRemakeList = {};
  Future getSongRemake(musicId, offset) {
    return SongApi.songRemake(musicId, limit: offset * 20);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      MusicInfoModel provider =
        Provider.of<PlayingStatus>(context, listen: false).hasPlayingSongs;
      getSongRemake(provider?.songs[0]?.id, offset).then((value) {
        setState(() {
          remakeModel = RemakeModel.fromJson(value);
          musicDetail = provider;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return musicDetail?.songs == null
        ? Center(
            child: Loading(
              color: Colors.blueAccent,
              text: '加载中。。。',
            ),
          )
        : Scaffold(
            backgroundColor: Color(0xff1d1726),
            appBar: AppBar(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    musicDetail?.songs == null
                        ? ''
                        : musicDetail?.songs[0]?.name,
                    style: TextStyle(color: Colors.grey, fontSize: 16),
                  ),
                  Text(
                    '${musicDetail?.songs[0]?.ar[0]?.name}-${musicDetail?.songs[0]?.al?.name}',
                    style: TextStyle(color: Colors.grey, fontSize: 12),
                  )
                ],
              ),
              backgroundColor: Color(0xff1d1726),
            ),
            body: remakeModel?.hotComments == null
                ? Center(
                    child: Loading(
                      color: Colors.blueAccent,
                      text: '正在加载评论...',
                    ),
                  )
                : ListView.builder(
                    itemCount: remakeModel?.hotComments?.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Column(
                        children: [
                          ListTile(
                            leading: CircleAvatar(
                              backgroundColor: Colors.grey,
                              backgroundImage: NetworkImage(remakeModel
                                  ?.hotComments[index]?.user?.avatarUrl),
                            ),
                            title: Text(
                              remakeModel?.hotComments[index]?.user?.nickname,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 12),
                            ),
                            subtitle: Text(
                              "${DateTime.fromMillisecondsSinceEpoch(remakeModel?.hotComments[index]?.time).toString().substring(0, 10)}",
                              style: TextStyle(color: Colors.grey, fontSize: 8),
                            ),
                            trailing: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                LikeButton(
                                  likeBuilder: (bool isLiked){
                                    return Icon(Icons.thumb_up_alt_outlined,color: isLiked ? Colors.red : Colors.grey,);
                                  },
                                  likeCount:remakeModel?.hotComments[index]?.likedCount,
                                  circleColor:CircleColor(start: Color(0xff00ddff), end: Color(0xff0099cc)),
                                  bubblesColor: BubblesColor(
                                    dotPrimaryColor: Color(0xff33b5e5),
                                    dotSecondaryColor: Color(0xff0099cc),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width / 5,0, 0, 15),
                            width: MediaQuery.of(context).size.width,
                            child: Text(
                              "${remakeModel?.hotComments[index]?.content}",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 12),
                            ),
                          ),
                          Divider(
                            height: 0.5,
                            color: Color(0xFF616161),
                          ),
                        ],
                      );
                    }),
          );
  }
}
