import 'package:LifeMusic/provider/userProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// https://www.jianshu.com/p/ca79eb54a356
// https://www.jianshu.com/p/7ebccca5f8a8
// https://blog.csdn.net/yechaoa/article/details/90701321
// https://blog.csdn.net/weixin_33767813/article/details/91425103
class UserCenter extends StatefulWidget {
  @override
  _UserCenterState createState() => _UserCenterState();
}

class _UserCenterState extends State<UserCenter> {
  TabController _tabController;

  var _tabs = <String>[
    "主页",
    "动态",
    "评论",
  ];
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: _tabs.length, // This is the number of tabs.
        child: Consumer<UserState>(builder: (context, user, child) {
          return NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverOverlapAbsorber(
                  handle:
                  NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                  sliver: SliverAppBar(
                    backgroundColor:  Color(0xff1d1726),
                    centerTitle: false,
                    floating: false,
                    snap: false,
                    primary: true,
                    expandedHeight: 300.0,
                    elevation: 10,
                    // forceElevated: true, // 强制显示阴影
                    // floating: true,// 设置该属性，当有下滑手势的时候，就会显示 AppBar
                    // 该属性只有在 floating 为 true 的情况下使用，不然会报错
                    // 当上滑到一定的比例，会自动把 AppBar 收缩（不知道是不是 bug，当 AppBar 下面的部件没有被 AppBar 覆盖的时候，不会自动收缩）
                    // 当下滑到一定比例，会自动把 AppBar 展开
                    // snap: true,// 设置该属性使 Appbar 折叠后不消失
                    pinned: true, // 通过这个属性设置 AppBar 的背景
                    stretch: true,
                    automaticallyImplyLeading: true,
                    //是否显示阴影，直接取值innerBoxIsScrolled，展开不显示阴影，合并后会显示
                    forceElevated: innerBoxIsScrolled,
                    actions: <Widget>[
                      new IconButton(
                        icon: Icon(Icons.share_outlined),
                        onPressed: () {
                          print("更多");
                        },
                      ),
                    ],
                    flexibleSpace: new FlexibleSpaceBar(
                      // background: Image.asset("assets/wallpaper/sunset-mountains-0.jpg", fit: BoxFit.cover),
                      background: Container(
                        decoration: BoxDecoration(
                          image: new DecorationImage(
                              fit: BoxFit.cover,
                              image: new NetworkImage(user?.user?.profile?.backgroundUrl)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(10, 30, 10, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              ClipOval(
                                //圆形头像
                                child: new Image.network(
                                  '${user?.user?.profile?.avatarUrl}',
                                  width: 80.0,
                                ),
                              ),
                              Container(
                                width: 80,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      '${user?.user?.profile?.nickname}',
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Text(
                                            "关注${user?.user?.profile?.follows}",style: TextStyle(
                                            color: Colors.grey,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12)),
                                        Text(
                                            "粉丝${user?.user?.profile?.followeds}",style: TextStyle(
                                            color: Colors.grey,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 12))
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Icon(Icons.twelve_mp),
                                  ),
                                  Wrap(
                                    direction: Axis.horizontal, // 主轴方向为水平方向
                                    spacing: 30.0, // 主轴方向上子项之间的距离
                                    runSpacing: 2.0, // 次轴方向上子项之间的距离
                                    alignment: WrapAlignment.end, //按主轴末端对齐
                                    runAlignment:
                                    WrapAlignment.center, // 次轴居中显示
                                    crossAxisAlignment:
                                    WrapCrossAlignment.start, //按次轴起始端对齐
                                    // textDirection: TextDirection.rtl,// 子项在水平方向上从右到左
                                    verticalDirection:
                                    VerticalDirection.up, // 子项在垂直方向上从下到上排列
                                    children: <Widget>[
                                      Chip(
                                        avatar: CircleAvatar(
                                            backgroundColor: Colors.grey,
                                            child: Icon(
                                              Icons.edit_road_outlined,
                                              color: Colors.red,
                                            )),
                                        label: Text('编辑'),
                                      ),
                                      Chip(
                                        avatar: CircleAvatar(
                                            backgroundColor: Colors.grey,
                                            child: new Text(
                                              'B',
                                              style: TextStyle(fontSize: 10.0),
                                            )),
                                        label: Text('更换背景'),
                                      ),
                                    ],
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    bottom: TabBar(
                      controller: this._tabController,
                      labelColor: Colors.redAccent,
                      labelStyle:
                      TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                      unselectedLabelColor: Colors.grey, //未选中的颜色//选中的样式
                      indicatorColor: Colors.transparent, //下划线颜色
                      indicatorSize:
                      TabBarIndicatorSize.label, //指示器和题目等长，tab不等长。
                      isScrollable: false, //是否可滑动，设置不可滑动，则是tab的宽度等长
                      tabs:
                      _tabs.map((String name) => Tab(text: name)).toList(),
                    ),
                  ),
                ),
              ];
            },
            body: Column(
              children: <Widget>[
                Expanded(
                  child: TabBarView(
                      controller: this._tabController,
                      children: <Widget>[
                        Center(child: Text('Content of Home')),
                        Center(child: Text('Content of Home')),
                        Center(child: Text('Content of Profile')),
                      ]),
                ),
              ],
            ),
          );
        }));
  }
}
