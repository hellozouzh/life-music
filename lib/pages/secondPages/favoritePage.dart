import 'dart:convert';
import 'dart:ui';
import 'package:LifeMusic/pages/secondPages/userDrawer.dart';
import 'package:LifeMusic/routers/application.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:LifeMusic/api/songListApi.dart';
import 'package:LifeMusic/utils/localStorage.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vant_kit/widgets/loading.dart';

class FavoritePage extends StatefulWidget {
  const FavoritePage({Key key}) : super(key: key);

  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  List _list = [];
  Future playList(uid) async {
    var result = await SongApi.getPlayList(uid);
    return result;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    LocalStorage.getString('userInfo').then((val) {
      if (!val.isEmpty) {
        playList(jsonDecode(val)["profile"]["userId"]).then((value) {
          if (mounted) {
            setState(() {
              _list = value['playlist'];
            });
          }
        });
      } else {}
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerHeaderWidget(),
      key: _scaffoldKey,
      backgroundColor: Color(0xff1d1726),
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.list,color: Colors.white,), onPressed:(){
          _scaffoldKey.currentState.openDrawer();
        }),
        actions: [
          IconButton(icon: Icon(Icons.settings_voice,color: Colors.white,), onPressed: null)
        ],
        backgroundColor: Color(0xff1d1726),
        title: Center(
          child: Container(
            height: 35,
            // margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
            child: TextField(
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                focusColor: Colors.red,
                contentPadding: const EdgeInsets.symmetric(vertical: 4.0),
                hintText: '请输入搜索内容',
                hintStyle: TextStyle(color: Colors.white),
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.grey,
                ),
                // contentPadding: EdgeInsets.all(10),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: BorderSide.none),
                filled: true,
                fillColor: Color(0xff2d2835),
              ),
            ),
          ),
        ),
      ),
      body: Column(
        children: [
          titleContainer(title: "我的最爱"),
          Container(
              height: 180,
              child: _list.length == 0
                  ? Center(
                      child: Loading(
                        color: Colors.blueAccent,
                        text: '正在努力加载中...',
                      ),
                    )
                  : ListView.builder(
                      itemCount: _list.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context, int index) {
                        return songCard(_list[index]);
                      })),
          Container(
            height: 150,
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 1,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Container(
                        decoration: BoxDecoration(
                          gradient: const LinearGradient(colors: [
                            Color(0xfff77d73),
                            Color(0xfffa639d),
                            Color(0xfffc51bb),
                          ]),
                        ),
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: Padding(
                                padding: EdgeInsets.all(15),
                                child: Text(
                                  '本地音乐',
                                  style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Padding(
                                  padding: EdgeInsets.all(15),
                                  child: Icon(
                                    Icons.my_library_music_rounded,
                                    color: Colors.white,
                                    size: 30,
                                  )),
                            ),
                          ],
                        )),
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      clipRRectContainer(
                          radius: 10,
                          childHeight: 60,
                          gradientColor: [
                            Color(0xff9b78f0),
                            Color(0xff8e63e3),
                            Color(0xff804ad3),
                          ],
                          alignment: Alignment.topLeft,
                          alignPadding: 15,
                          text: "最近播放",
                          textSize: 16,
                          icon: Icons.history),
                      clipRRectContainer(
                          radius: 10,
                          childHeight: 60,
                          gradientColor: [
                            Color(0xff58c7d2),
                            Color(0xff52b8cb),
                            Color(0xff4ba4c3),
                          ],
                          alignment: Alignment.bottomRight,
                          alignPadding: 15,
                          text: "每日推荐",
                          textSize: 16,
                          icon: Icons.recommend)
                    ],
                  ),
                ),
              ],
            ),
          ),
          titleContainer(title:'收藏的歌单')
        ],
      ),
    );
  }

  Widget clipRRectContainer(
      {double radius,
      double childHeight,
      List<Color> gradientColor,
      AlignmentGeometry alignment,
      double alignPadding,
      String text,
      double textSize,
      IconData icon}) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius),
      child: Container(
        height: childHeight,
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: gradientColor),
        ),
        child: Align(
          alignment: alignment,
          child: Padding(
            padding: EdgeInsets.all(alignPadding),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '$text',
                  style: TextStyle(
                      fontSize: textSize,
                      fontWeight: FontWeight.w800,
                      color: Colors.white),
                ),
                Icon(
                  icon,
                  color: Colors.white,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget songCard(item) {
    return GestureDetector(
      onTap: () {
        Application.router.navigateTo(
          context,
          "/songListPage?listId=${item["id"].toString()}",
        );
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(15.0, 15.0, 10.0, 15.0),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child: Stack(
              children: [
                Container(
                  width: 120,
                  height: 150,
                  child: FadeInImage.assetNetwork(
                      placeholder: 'assets/rhomb-white.gif',
                      image: '${item["coverImgUrl"]}',
                      fit: BoxFit.fill),
                ),
                Positioned(
                  bottom: 0,
                  child: Container(
                    width: 120,
                    height: 35,
                    color: Color.fromRGBO(63, 58, 69, 0.8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '${item["name"]}',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(
                          '${item["trackCount"]}首',
                          style: TextStyle(color: Colors.white, fontSize: 10),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            )),
      ),
    );
  }
  Widget titleContainer({String title}){
    return Container(
      margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            child: Text(
              '${title}',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
            ),
          ),
          FlatButton(
              color: Color(0xff11f1f1),
              highlightColor: Color(0xff00ff00),
              onPressed: null,
              child: Text('More',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Colors.red,
                  )))
        ],
      ),
    );
  }
}
