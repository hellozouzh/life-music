import 'package:flutter/material.dart';

class IndexPage extends StatefulWidget {
  final String url;
  final String title;
  const IndexPage({Key key, this.url, this.title}) : super(key: key);

  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${widget.title}'),
      ),
    );
  }
}
