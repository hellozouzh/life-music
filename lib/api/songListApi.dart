import 'httpRequest.dart';
class SongApi{

  //获取用户歌单
  static getPlayList(int uid ,{int limit=30,  int offset}){
   return HttpUtil().post("/user/playlist",data: {"uid": uid, "limit":limit, "offset":offset});
  }

  //获取歌单详情
  static getPlayListDetail(int id,{int s=8}){
    return HttpUtil().post('/playlist/detail',data: {
      "id":id,
      "s":s
    });
  }

  //使用trackIds 获取歌曲
  static getSongList(String ids){
    return HttpUtil().post('/song/detail',data: {
      "ids":ids
    });
  }
  //获取音乐url
  static getMusicUrl(String id,{String type="lyric", int br=320000}){
    return HttpUtil().post('/song/url',data: {
      "id":id,
      "type":type,
      "br":br
    });
  }
  //获取音乐详情
  static getMusicDetail(String ids){
    return HttpUtil().post("/song/detail",data: {
      "ids":ids
    });
  }
  //获取歌词
  static Lyric(String id,{String type="lyric"}){
    return HttpUtil().post('https://api.imjad.cn/cloudmusic/',data: {
      "type":type,
      "id":id,
    });
  }
  static songRemake(int id,{int limit=20}){
    return HttpUtil().post('/comment/music',data: {
      "id":id,
      "limit":limit,
    });
  }
}