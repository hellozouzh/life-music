import 'httpRequest.dart';

class GetUserInfoAPi {
  //名言警句
  static famousWord(){
    return HttpUtil().get("https://v1.hitokoto.cn/?c=d&c=i&encode=json");
  }

  //手机号登录
  static loginByPhone(String phone,
      {String password,int countrycode, String md5_password = ''}) {
    return HttpUtil().post(
      '/login/cellphone',
      data: {
        "phone": phone,
        "password": password,
        "md5_password": md5_password
      },
    );
  }

  //邮箱登录
  static loginByEmail(String email, {String md5_password = ''}) {
    return HttpUtil().post(
      '/login',
      data: {
        "email": email,
        "md5_password": md5_password,
      },
    );
  }

  //发送验证码
  static sendSMS(int phone) {
    return HttpUtil().post('/captcha/sent', data: {"phone": phone});
  }

  //验证验证码
  static verifySMS(int phone, int captcha) {
    return HttpUtil()
        .post('/captcha/verify', data: {"phone": phone, "captcha": captcha});
  }

  //注册(修改密码)
  static register(int captcha, int phone, String password, String nickname) {
    return HttpUtil().post('/register/cellphone', data: {
      "captcha": captcha,
      "phone": phone,
      "password": password,
      "nickname": nickname
    });
  }

  //检测手机号码是否已注册
  static checkPhone(int phone) {
    return HttpUtil()
        .post("/cellphone/existence/check", data: {"phone ": phone});
  }

  //退出
  static logout() {
    return HttpUtil().post("/cellphone/existence/check");
  }

  //获取用户详情
  static userInfo(int uid) {
    return HttpUtil().post("/user/detail", data: {"uid": uid});
  }

  //获取banner轮播图数据
  static banner(int type){
    return HttpUtil().post('/banner', data: {"type":type});
  }

  //获取用户信息 , 歌单，收藏，mv, dj 数量
  static userSubcount() {
    return HttpUtil().post("/user/subcount");
  }

  //获取用户等级信息
  static userLevel() {
    return HttpUtil().post("/user/level");
  }

  //获取用户绑定信息
  static getBindINfo(int uid) {
    return HttpUtil().post("/user/binding", data: {"uid ": uid});
  }
  //更新头像
  // static updateHead(){
  //   return HttpUtil().post('')
  // }

  //更新用户信息
  static updateInfo(int gender, String birthday, String nickname, int province,
      int city, String signature) {
    return HttpUtil().post("/user/update", data: {
      "gender": gender,
      "birthday": birthday,
      "nickname": nickname,
      "province": province,
      "city": city,
      "signature": signature
    });
  }
}
