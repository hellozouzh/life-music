import 'dart:convert';

class FluroParams {
  static String encode(String originalCn) {
    return jsonEncode(Utf8Encoder().convert(originalCn));
  }

  static String decode(String encodeCn) {
    var list = List<int>();
    ///字符串解码
    for(var data in jsonDecode(encodeCn)[0]){
      list.add(data);
    }
    return Utf8Decoder().convert(list);
  }
}