import 'package:fluro/fluro.dart';
import './router_handler.dart';

class Routes {
  // static String root = '/';
  static String index = '/indexPage';
  static String home = '/homePage';

  static String loginType = '/loginTypePage';
  static String register = '/registerPage';
  static String login = '/loginPage';
  static String songList  ='/songListPage';
  static String songPlayer='/songPlayerPage';
  static String remakeList='/remakeList';
  static String userCenter='/userCenter';


  static void configureRoutes(FluroRouter router) {
    // router.notFoundHandler = new Handler(
    //   handlerFunc: (BuildContext context, Map<String, List<String> params) {
    //     print('ERROR====>ROUTE WAS NOT FONUND!!!'); // 找不到路由，跳转404页面
    //     print('找不到路由，404');
    //   }
    // );

    // 路由页面配置
    router.define(index, handler: indexPageHandler,transitionType: TransitionType.fadeIn);
    router.define(home, handler: homePageHandler,transitionType: TransitionType.inFromBottom);
    router.define(loginType, handler: loginTypePageHandler);
    router.define(register, handler: registerPageHandler);
    router.define(login, handler: loginPageHandler);
    router.define(songList, handler: songListPageHandler);
    router.define(songPlayer, handler: songPlayerHandler);
    router.define(remakeList, handler: remakeListHandler);
    router.define(userCenter, handler: userCenterHandler);
    //
  }
}
