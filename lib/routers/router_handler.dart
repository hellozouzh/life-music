import 'package:LifeMusic/pages/UserCenter/userCenter.dart';
import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';

import '../pages/indexPage.dart';
import '../pages/home.dart';
import '../pages/login/loginType.dart';
import '../pages/login/login.dart';
import '../pages/login/register.dart';
import '../pages/secondPages/songListPage.dart';
import '../pages/player/player.dart';
import '../pages/player/remark.dart';
/* *
 * handler就是每个路由的规则，编写handler就是配置路由规则，
 * 比如我们要传递参数，参数的值是什么，这些都需要在Handler中完成。
 */

// 首页
Handler indexPageHandler = Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) {
    String title = params['title']?.first;
    String url = params['url']?.first;
    return IndexPage(title: title, url: url);
  },
);

// 主页
Handler homePageHandler = Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) {
    return HomePage();
  },
);

//选择登录方式
Handler loginTypePageHandler = Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) {
    String type=params['type']?.first;
    return LoginTypePage();
  },
);

//注册
Handler registerPageHandler = Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) {
    return RegisterPage();
  },
);

// 登录
Handler loginPageHandler = Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) {
    String type= params['type']?.first;
    return LoginPage(type: type,);
  },
);

//进入歌单
Handler songListPageHandler = Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) {
    var listId= params['listId']?.first;
    return SongListPage(listId: "${listId}",);
  },
);

//进入歌曲播放界面
Handler songPlayerHandler = Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) {
    String musicId=params['musicId']?.first;
    return SongPlayPage(musicId:musicId);
  },
);

//进入评论界面
Handler remakeListHandler = Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) {
    return RemakeList();
  },
);

//进入用户中心
Handler userCenterHandler = Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) {
    return UserCenter();
  },
);