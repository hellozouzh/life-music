import 'package:LifeMusic/model/musicInfoModel.dart';
import 'package:LifeMusic/model/musicPlayingListModel.dart';
import 'package:flutter/material.dart';

class PlayingStatus with ChangeNotifier {
  bool musicPlayStatus = false;
  int musicPlayIndex = 0;
  MusicInfoModel playingMusicInfo = MusicInfoModel();
  MusicPlayingList musicPlayingList = MusicPlayingList();
  Map<String, dynamic> _musicPlayingList = {"songs": []};
  String musicPlayTime;

  //当前音乐的播放状态
  get hasPlaying => musicPlayStatus;
  set hasPlaying(bool state) {
    musicPlayStatus = state;
    notifyListeners();
  }

  //当前播放的音乐的索引
  get musicIndex => musicPlayIndex;
  set musicIndex(int index) {
    musicPlayIndex = index;
  }

  //当前正在播放的歌曲
  get hasPlayingSongs => playingMusicInfo;
  set hasPlayingSongs(MusicInfoModel music) {
    playingMusicInfo = music;
    notifyListeners();
  }

  //当前正在播放的歌曲队列
  get playingList => MusicPlayingList.fromJson(_musicPlayingList);
  set playingList(List<dynamic> musicList) {
    _musicPlayingList["songs"].addAll(musicList);
    notifyListeners();
  }

  get playTime => musicPlayStatus;
  set playTime(String time) {
    musicPlayTime = time;
    notifyListeners();
  }
}
