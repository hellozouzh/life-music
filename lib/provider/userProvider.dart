import 'package:LifeMusic/model/userInfoModel.dart';
import 'package:flutter/material.dart';

class UserState with ChangeNotifier{
      //登录状态
      bool loginState =false;
      bool get hasLogin =>loginState;
      set hasLogin(bool state){
        loginState =state;
        notifyListeners();
      }
      //用户信息
      UserInfoModel user;
      get userInfo=>user;

      set userInfo(Map<String,dynamic> info){
          user=UserInfoModel.fromJson(info);
          notifyListeners();
      }
}