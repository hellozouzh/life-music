import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import 'package:provider/provider.dart';
import 'package:LifeMusic/provider/userProvider.dart';
import 'package:LifeMusic/provider/musicPlayingProvider.dart';

import 'package:fluro/fluro.dart';
import 'package:LifeMusic/routers/routes.dart';
import 'package:LifeMusic/routers/application.dart';
import 'package:LifeMusic/utils/localStorage.dart';

import 'package:LifeMusic/pages/login/loginType.dart';
import 'package:LifeMusic/pages/home.dart';



void main() {
  //-------------------路由主要代码start
  final router = FluroRouter();
  Routes.configureRoutes(router);
  Application.router = router;
  //-------------------路由主要代码end
  runApp(MyApp());
  if(Platform.isAndroid){
    SystemUiOverlayStyle style = SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        ///这是设置状态栏的图标和字体的颜色
        //Brightness.light  //一般都是显示为白色
        //Brightness.dark, //一般都是显示为黑色
        statusBarIconBrightness: Brightness.dark
    );
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
  }

}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: UserState()),
          ChangeNotifierProvider.value(value: PlayingStatus()),
        ],
        child: Consumer<UserState>(builder: (context, user, child) {
          return MaterialApp(
              title: 'Flutter Demo',
              onGenerateRoute: Application.router.generator,
              theme: ThemeData(
                  primarySwatch: Colors.blue,
                  // visualDensity: VisualDensity.adaptivePlatformDensity,
                  ),
              builder: (BuildContext context, Widget child) {
                return FlutterEasyLoading(child: child);
              },
              home: Home());
        }));
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Map<String, dynamic> account = {};
  bool hasLogin = false;
  @override
  void initState() {
    LocalStorage.getString('userInfo').then((value) {
      if (value.isEmpty) {
        setState(() {
          hasLogin = false;
        });
      } else {
        setState(() {
          account = jsonDecode(value);
          hasLogin = true;
        });
      }
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserState>(builder: (context, user, child) {
      if (hasLogin) {
        user.loginState = true;
        user.userInfo = account;
        return HomePage();
      } else {
        return LoginTypePage();
      }
    });
  }
}
